extends Node

onready var end_turn_btn = $Sidebar/VBoxContainer/CenterContainer2/BtnEndTurn
onready var new_game_btn = $Sidebar/VBoxContainer/CenterContainer2/BtnNewGame
onready var popup = $CanvasLayer/Popup

func _ready():
#    get_node("/root").set_size_override(true, Vector2(240, 240))
    OS.set_window_size(Vector2(1280, 720))
    $World.connect("lose", self, "lose")
    $World.connect("update_sidebar", self, "update_sidebar")
    $World.connect("next_level", self, "next_level")
    $World.connect("end_screen", self, "end_screen")
    update_sidebar()
    popup.opening_screen()

func lose():
    popup.lose_game()
    end_turn_btn.hide()
    new_game_btn.show()
    update_sidebar()

func update_sidebar():
    $Sidebar.update_sidebar()

func next_level():
    print("next level")
    G.resolving = false
    G.turn_number  = 0
    G.term_number += 1
    G.votes_against = 0
    G.votes_for = 0
    G.lost_level = false
    end_turn_btn.show()
    new_game_btn.hide()
    $World.start_level()
    update_sidebar()

func new_game():
    print("new game")
    G.resolving = false
    G.turn_number = 1
    G.term_number = 1
    G.votes_against = 0
    G.votes_for = 0
    G.votes_for_total = 0
    G.lost_level = false
    end_turn_btn.show()
    new_game_btn.hide()
    $World.new_game()
    popup.new_game()

func end_screen():
    popup.end_screen()
    end_turn_btn.hide()
    new_game_btn.show()
    update_sidebar()
