extends AnimatedSprite

const BoatAbility = preload("res://src/BoatAbility.gd").BoatAbility

enum PowerUp {
    BASE,
    DIAGONAL,
    JET,
    TURBO
}

var powerup

func _ready():
    var powerup_roll = randi() % 4
    if powerup_roll == 0:
        set_powerup(PowerUp.BASE)
    elif powerup_roll == 1:
        set_powerup(PowerUp.DIAGONAL)
    elif powerup_roll == 2:
        set_powerup(PowerUp.JET)
    elif powerup_roll == 3:
        set_powerup(PowerUp.TURBO)

func set_powerup(p_powerup):
    self.powerup = p_powerup
    if p_powerup == PowerUp.DIAGONAL:
        self.modulate = Color(0, 1, 0)
    if p_powerup == PowerUp.JET:
        self.modulate = Color(0, 0, 1)
    if p_powerup == PowerUp.TURBO:
        self.modulate = Color(1, 0, 0)
    if p_powerup == PowerUp.BASE:
        self.modulate = Color(1, 1, 1)
