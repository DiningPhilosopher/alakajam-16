# spawn areas for each term
const spawn_areas = [
    # TERM 1
    [
        {
            "topleft": [2, 6],
            "size": [1, 2]
        }, 
        {
            "topleft": [4, 3],
            "size": [2, 2],
            "max_tiles": 2
        }, 
        {
            "topleft": [7, 6],
            "size": [1, 2]
        }, 
        {
            "topleft": [4, 9],
            "size": [2, 2],
            "max_tiles": 2
        }
    ],
    # TERM 2
    [
        {
            "topleft": [1, 5],
            "size": [2, 4],
            "max_tiles": 5
        },
        {
            "topleft": [2, 10],
            "size": [3, 1],
            "max_tiles": 1
        },
        {
            "topleft": [6, 10],
            "size": [2, 2],
            "max_tiles": 2
        },
        {
            "topleft": [7, 5],
            "size": [2, 4],
            "max_tiles": 2
        },
        {
            "topleft": [3, 2],
            "size": [4, 3],
            "max_tiles": 3
        },
    ],
    # TERM 3
    [
        {
            "topleft": [2, 2],
            "size": [2, 3],
            "max_tiles": 3
        },
        {
            "topleft": [2, 6],
            "size": [2, 2],
        }, 
        {
            "topleft": [2, 9],
            "size": [2, 3],
            "max_tiles": 3
        }, 
        {
            "topleft": [6, 2],
            "size": [2, 3],
            "max_tiles": 2
        }, 
                {
            "topleft": [6, 6],
            "size": [2, 2]
        }, 
                {
            "topleft": [6, 9],
            "size": [2, 3],
            "max_tiles": 2
        }, 
    ],
    # TERM 4
    [
        {
            "topleft": [1, 3],
            "size": [2, 2],
            "max_tiles": 1
        },
        {
            "topleft": [1, 6],
            "size": [3, 2],
            "max_tiles": 2
        },
        {
            "topleft": [1, 9],
            "size": [2, 2],
            "max_tiles": 1
        },
        {
            "topleft": [4, 11],
            "size": [2, 2],
            "max_tiles": 2
        },
        {
            "topleft": [7, 11],
            "size": [2, 2],
            "max_tiles": 1
        },
        {
            "topleft": [7, 4],
            "size": [2, 6],
            "max_tiles": 2
        },
        {
            "topleft": [4, 1],
            "size": [3, 3],
            "max_tiles": 3
        },
    ],
    # TERM 5
    [
        {
            "topleft": [1, 1],
            "size": [2, 2],
            "max_tiles": 1
        },
        {
            "topleft": [1, 4],
            "size": [3, 3],
            "max_tiles": 2
        },
        {
            "topleft": [1, 8],
            "size": [3, 3],
            "max_tiles": 2
        },
        {
            "topleft": [3, 12],
            "size": [2, 1],
            "max_tiles": 1
        },
        {
            "topleft": [5, 9],
            "size": [1, 2],
            "max_tiles": 1
        },
        {
            "topleft": [7, 11],
            "size": [2, 2],
            "max_tiles": 2
        },
        {
            "topleft": [6, 6],
            "size": [3, 3],
            "max_tiles": 2
        },
        {
            "topleft": [4, 2],
            "size": [1, 2],
            "max_tiles": 1
        },
    ]
]
