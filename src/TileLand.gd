extends "res://src/Tile.gd"

const TownScene = preload("res://scenes/Town.tscn")

# for party boat
var partying = false


func add_town():
    if self.town != null:
        return self.town
    self.town = TownScene.instance()
    self.town.position = Vector2(0, -0.2 * G.TILE_DIM)
    self.add_child(self.town)
    return self.town

func set_partying():
    print("parte")
    partying = true
    self.town.party()

func fix_sprite(neighs):
    self.animation = animation_map.get(type)
    if self.town != null:
        if self.type == TileType.LAND:
            self.town.visible = true
        else:
            self.town.visible = false
    if self.type != TileType.LAND:
        # Hide land tiles when they're not... land.
        self.self_modulate = Color(1, 1, 1, 0)
        return
    self.self_modulate = Color(1, 1, 1, 1)
    var up = neighs[0][1].type == TileType.LAND
    var right = neighs[1][2].type == TileType.LAND
    var down = neighs[2][1].type == TileType.LAND
    var left = neighs[1][0].type == TileType.LAND
    var ur = neighs[0][2].type == TileType.LAND
    var dr = neighs[2][2].type == TileType.LAND
    var ul = neighs[0][0].type == TileType.LAND
    var dl = neighs[2][0].type == TileType.LAND
    if not up and not right and not down and not left:
        frame = 0
    if up and not right and not down and not left:
        frame = 1
    if not up and right and not down and not left:
        frame = 2
    if not up and not right and down and not left:
        frame = 3
    if not up and not right and not down and left:
        frame = 4
    if up and right and not down and not left:
        if ur: frame = 6
        else: frame = 5
    if up and not right and not down and left:
        if ul: frame = 8
        else: frame = 7
    if not up and right and down and not left:
        if dr: frame = 10
        else: frame = 9
    if not up and not right and down and left:
        if dl: frame = 12
        else: frame = 11
    if not up and right and not down and left:
        frame = 13
    if up and not right and down and not left:
        frame = 14
    if up and right and down and not left:
        if not ur and not dr: frame = 15
        if ur and not dr: frame = 16
        if dr and not ur: frame = 17
        if ur and dr: frame = 18
    if up and right and not down and left:
        if not ul and not ur: frame = 19
        if ul and not ur: frame = 20
        if ur and not ul: frame = 21
        if ul and ur: frame = 22
    if up and not right and down and left:
        if not ul and not dl: frame = 23
        if ul and not dl: frame = 24
        if dl and not ul: frame = 25
        if ul and dl: frame = 26
    if not up and right and down and left:
        if not dl and not dr: frame = 27
        if dl and not dr: frame = 28
        if dr and not dl: frame = 29
        if dl and dr: frame = 30
    if up and right and down and left:
        if not ur and not dr and not dl and not ul: frame = 31
        if ur and not dr and not dl and not ul : frame = 32
        if not ur and dr and not dl and not ul: frame = 33
        if not ur and not dr and dl and not ul: frame = 34
        if not ur and not dr and not dl and ul: frame = 35
        if ur and not dr and not dl and ul: frame = 36
        if ur and dr and not dl and not ul: frame = 37
        if not ur and dr and dl and not ul: frame = 38
        if not ur and not dr and dl and ul: frame = 39
        if ur and not dr and dl and not ul: frame = 40
        if not ur and dr and not dl and ul: frame = 41
        if ur and dr and not dl and ul: frame = 42
        if ur and dr and dl and not ul: frame = 43
        if not ur and dr and dl and ul: frame = 44
        if ur and not dr and dl and ul: frame = 45
        if ur and dr and dl and ul: frame = 46
