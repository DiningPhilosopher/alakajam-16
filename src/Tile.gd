extends AnimatedSprite

const TileType = preload("res://src/TileType.gd").TileType


var type = TileType.WATER
var r
var c
var island
var town = null
var boat = null

# helper for pathfinding
var distance = null

const animation_map  = {
    TileType.DEEPWATER: "deepwater",
    TileType.WATER: "water",
    TileType.LAND: "land"
   }

func _init():
    self.animation = animation_map.get(type)

func initialize(p_r, p_c):
    var center_offset = 0.5 * G.TILE_DIM * Vector2(1, 1)
    self.r = p_r
    self.c = p_c
    self.position = center_offset + Vector2(self.c * G.TILE_DIM, self.r * G.TILE_DIM)

func set_type(p_type):
    self.type = p_type

func place_boat(p_boat):
    self.boat = p_boat
    self.boat.position = self.position
    if boat.tile != null:
        boat.tile.boat = null
    boat.tile = self

func select():
    print("selected at " + str(self.r) + " " + str(self.c))
