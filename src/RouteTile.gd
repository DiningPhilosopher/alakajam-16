extends Sprite

var r
var c

# helper for pathfinding
var distance

func initialize(p_r, p_c):
    var center_offset = 0.5 * G.TILE_DIM * Vector2(1, 1)
    self.r = p_r
    self.c = p_c
    self.position = center_offset + Vector2(self.c * G.TILE_DIM, self.r * G.TILE_DIM)
    self.visible = false
