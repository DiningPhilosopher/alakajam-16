extends AnimatedSprite

const BoatAbility = preload("res://src/BoatAbility.gd").BoatAbility
const TileType = preload("res://src/TileType.gd").TileType

var move_done = false
var speed = 4
var tile = null

var abilities = [
    BoatAbility.DIAGONAL,
]

func add_ability(ability):
    if not ability in abilities:
        abilities.append(ability)
        if ability == BoatAbility.DREDGE:
            self.animation = "dredge"
        if ability == BoatAbility.PARTY:
            self.animation = "party"
        if ability == BoatAbility.DRILL:
            self.animation = "drill"

func do_actions(world):
    for ability in abilities:
        if ability == BoatAbility.DRILL:
            drill(self.tile.r, self.tile.c, world)
        if ability == BoatAbility.DREDGE:
            dredge(self.tile.r, self.tile.c, world)
        if ability == BoatAbility.PARTY:
            party(self.tile.r, self.tile.c, world)

func drill(r, c, world):
    if world.future_tiles[r][c].type == TileType.LAND:
        world.future_tiles[r][c].set_type(TileType.WATER)
        world.move_tiles[r][c].set_type(TileType.WATER)

func dredge(r, c, world):
    var move_neighs = world.neighbors_4(r, c, world.move_tiles)
    for neigh in move_neighs:
        if neigh.type == TileType.LAND and neigh.town == null:
            neigh.set_type(TileType.WATER)
            world.future_tiles[neigh.r][neigh.c].set_type(TileType.WATER)

func party(r, c, world):
    var neighs = world.neighbors_8(r, c, world.move_tiles)
    for n in neighs:
        if n.type == TileType.LAND and n.town != null:
            n.set_partying()
