extends Node2D

signal lose
signal update_sidebar
signal next_level
signal end_screen

const OneColorAnimationMaterial = preload("res://resources/OneColorAnimation.tres")

const BoatAbility = preload("res://src/BoatAbility.gd").BoatAbility
const TileType = preload("res://src/TileType.gd").TileType

const tile_scene = preload("res://scenes/Tile.tscn")
const tile_water_scene = preload("res://scenes/TileWater.tscn")
const tile_land_scene = preload("res://scenes/TileLand.tscn")
const route_tile_scene = preload("res://scenes/RouteTile.tscn")
const boat_scene = preload("res://scenes/Boat.tscn")

var islands = []
# Only a decorative backdrop
var water_tiles = []
# The layer that all boats "move" on
# And which shows the normal land sprites
var move_tiles = []
# Rising sea bottom, land preview
var future_tiles = []
# Markers for reachability
var route_tiles = []
var towns = []
var boats = []
var selected_boat = null

var timey = 0.0
var current_action = "shallows"

# Safe to use on border
func area_3x3_types(r, c, tilemap):
    var result = []
    # Default values, outside the map
    for _r in range(3):
        result.append([TileType.DEEPWATER, TileType.DEEPWATER, TileType.DEEPWATER])
    for dr in range(-1, 2):
        var row_idx = r + dr
        if row_idx >= 0 and row_idx < G.MAP_DIMS.y:
            for dc in range(-1, 2):
                var col_idx = c + dc
                if col_idx >= 0 and col_idx < G.MAP_DIMS.x:
                    result[dr + 1][dc + 1] = tilemap[row_idx][col_idx].type
    return result

# NOT safe to use on border
func area_3x3(r, c, tilemap):
    return [
        [tilemap[r - 1][c - 1], tilemap[r - 1][c], tilemap[r - 1][c + 1]],
        [tilemap[r][c - 1], tilemap[r][c], tilemap[r][c + 1]],
        [tilemap[r + 1][c - 1], tilemap[r + 1][c], tilemap[r + 1][c + 1]]
    ]

func neighbors_8(r, c, tilemap):
    var neighs = []
    # top row
    if r - 1 >= 0:
        if c - 1 >= 0:
            neighs.append(tilemap[r - 1][c - 1])
        neighs.append(tilemap[r - 1][c])
        if c + 1 < G.MAP_DIMS.x:
            neighs.append(tilemap[r - 1][c + 1])
    # center row
    if c - 1 >= 0:
        neighs.append(tilemap[r][c - 1])
    if c + 1 < G.MAP_DIMS.x:
        neighs.append(tilemap[r][c + 1])
    # bottom row
    if r + 1 < G.MAP_DIMS.y:
        if c - 1 >= 0:
            neighs.append(tilemap[r + 1][c - 1])
        neighs.append(tilemap[r + 1][c])
        if c + 1 < G.MAP_DIMS.x:
            neighs.append(tilemap[r + 1][c + 1])
    return neighs

func neighbors_4(r, c, tilemap):
    var neighs = []
    if r - 1 >= 0:
        neighs.append(tilemap[r - 1][c])
    if c + 1 < G.MAP_DIMS.x:
        neighs.append(tilemap[r][c + 1])
    if r + 1 < G.MAP_DIMS.y:
        neighs.append(tilemap[r + 1][c])
    if c - 1 >= 0:
        neighs.append(tilemap[r][c - 1])
    return neighs

func fix_border():
    for r in water_tiles:
        for tile in r:
            if tile.r == 0 or tile.r == G.MAP_DIMS.y - 1 \
                    or tile.c == 0 or tile.c == G.MAP_DIMS.x - 1:
                tile.set_type(TileType.DEEPWATER)
    var hide_border_tiles = [move_tiles, future_tiles]
    for hide_tiles in hide_border_tiles:
        for r in hide_tiles:
            for tile in r:
                if tile.r == 0 or tile.r == G.MAP_DIMS.y - 1 \
                        or tile.c == 0 or tile.c == G.MAP_DIMS.x - 1:
                    tile.visible = false

func add_tiles():
    water_tiles = []
    move_tiles = []
    future_tiles = []
    route_tiles = []
    for r in range(G.MAP_DIMS.y):
        water_tiles.append([])
        move_tiles.append([])
        future_tiles.append([])
        route_tiles.append([])
        for c in range(G.MAP_DIMS.x):
            var new_water_tile = tile_water_scene.instance()
            new_water_tile.initialize(r, c)
            var new_tile = tile_land_scene.instance()
            new_tile.initialize(r, c)
            var new_future_tile = tile_land_scene.instance()
            new_future_tile.initialize(r, c)
            new_future_tile.modulate = Color(0.0, 0.8, 0.0, 0.3)
            var new_route_tile = route_tile_scene.instance()
            new_route_tile.initialize(r, c)
            route_tiles[r].append(new_route_tile)
            water_tiles[r].append(new_water_tile)
            move_tiles[r].append(new_tile)
            future_tiles[r].append(new_future_tile)
            $land.add_child(new_future_tile)
            $land.add_child(new_tile)
            $water.add_child(new_water_tile)
            $above_land.add_child(new_route_tile)

func rand_tile_with_type_not_border(type, tilemap):
    var shuffled_rows = tilemap.duplicate()
    shuffled_rows.shuffle()
    for r in shuffled_rows:
        var shuffled_col_tiles = r.duplicate()
        shuffled_col_tiles.shuffle()
        for col_tile in shuffled_col_tiles:
            if col_tile.r != 0 and col_tile.r != G.MAP_DIMS.y - 1 \
                    and col_tile.c != 0 and col_tile.c != G.MAP_DIMS.x - 1 \
                    and col_tile.type == type:
                return col_tile
    return null

func gen_deepwater():
    var n_deepwater = floor(G.deepwater_fraction * (G.MAP_DIMS.x - 2) * (G.MAP_DIMS.y - 2))
    for rand_try in n_deepwater:
        var old_water = rand_tile_with_type_not_border(TileType.WATER, water_tiles)
        if old_water != null:
            old_water.set_type(TileType.DEEPWATER)
            move_tiles[old_water.r][old_water.c].set_type(TileType.DEEPWATER)
            future_tiles[old_water.r][old_water.c].set_type(TileType.DEEPWATER)
        else:
            break

func add_fish():
    var n_deepwater = floor(G.deepwater_fraction * (G.MAP_DIMS.x - 2) * (G.MAP_DIMS.y - 2))
    var n_fish_to_place = floor(G.fish_fraction * ((G.MAP_DIMS.x - 2) * (G.MAP_DIMS.y - 2) - n_deepwater))
    var fish_placed = 0
    while fish_placed < n_fish_to_place:
        var tile = rand_tile_with_type_not_border(TileType.WATER, water_tiles)
        if move_tiles[tile.r][tile.c].type == TileType.WATER and tile.add_fish():
            fish_placed += 1

func gen_islands():
    # [r, c] convention
    var spawn_areas = [
        {
            "topleft": [2, 2],
            "size": [2, 3]
        }, 
        {
            "topleft": [2, 6],
            "size": [2, 2]
        }, 
        {
            "topleft": [2, 9],
            "size": [2, 3]
        }, 
        {
            "topleft": [6, 2],
            "size": [2, 3]
        }, 
                {
            "topleft": [6, 6],
            "size": [2, 2]
        }, 
                {
            "topleft": [6, 9],
            "size": [2, 3]
        }, 
    ]
    spawn_areas.shuffle()
    var n_areas = 5
    for area_idx in range(n_areas):
        var a = spawn_areas[area_idx]
        var topleft = a["topleft"]
        var bottomright = [topleft[0] + a["size"][0], topleft[1] + a["size"][1]]
        var spawn_coord = a["topleft"]
        var new_spawn = spawn_coord.duplicate()
        new_spawn[0] += randi() % a["size"][0]
        new_spawn[1] += randi() % a["size"][1]
        var old_water = move_tiles[new_spawn[0]][new_spawn[1]]
        var town_placed = false
        for _extra_island in range(1 + randi() % 3):
            if old_water != null and old_water.type != TileType.LAND:
                old_water.set_type(TileType.LAND)
                if not town_placed:
                    town_placed = true
                    old_water.add_town()
                future_tiles[old_water.r][old_water.c].set_type(TileType.LAND)
            var next_tiles = neighbors_4(old_water.r, old_water.c, move_tiles)
            next_tiles.shuffle()
            for tile in next_tiles:
                if tile.type == TileType.WATER and \
                    tile.r >= topleft[0] and tile.r < bottomright[0] and \
                    tile.c >= topleft[1] and tile.c < bottomright[1]:
                        old_water = tile
                        break

func place_boats():
    var place_coord = Vector2(5, 5)
    var one_extra = 1 + randi() % 2
    var middle_boat = 1
    for boat in boats:
#        var move_tile = rand_tile_with_type_not_border(TileType.WATER, move_tiles)
        var move_tile = move_tiles[place_coord.y][place_coord.x]
        move_tile.place_boat(boat)
        place_coord += Vector2(1, 0)
        one_extra -= 1
        if one_extra == 0:
            place_coord += Vector2(1, 0)
        middle_boat -= 1
        if middle_boat == 0:
            place_coord -= Vector2(0, 1)
        else:
            place_coord += Vector2(0, 1)

func _input(event):
    if event is InputEventMouseButton and event.pressed:
        var pos = event.position
        var c = floor(pos.x / G.TILE_DIM)
        var r = floor(pos.y / G.TILE_DIM)
        if move_tiles != null and r >= 0 and r < len(move_tiles) \
                and c >= 0 and c < len(move_tiles[r]):
            var move_tile = move_tiles[r][c]
            move_tile.select()
            if event.button_index == BUTTON_LEFT:
                if move_tile.boat != null and not move_tile.boat.move_done:
                    selected_boat = move_tile.boat
                    self.highlight_reachables(move_tile.r, move_tile.c, move_tile.boat.speed, \
                            BoatAbility.DIAGONAL in move_tile.boat.abilities)
            elif event.button_index == BUTTON_RIGHT:
                if route_tiles[r][c].visible:  # The selected boat may go here
                    selected_boat.move_done = true
                    move_tiles[r][c].place_boat(selected_boat)
                    self.clear_reachables()

func clear_reachables():
    for r in route_tiles:
        for tile in r:
            tile.visible = false
            tile.distance = null

func sort_by_distance(a, b):
    return a.distance < b.distance

func highlight_reachables(r, c, speed, diagonal):
    clear_reachables()
    route_tiles[r][c].distance = 0
    var queue = []
    if speed >= 0:
        queue.append(route_tiles[r][c])
    while len(queue) > 0:
        queue.sort_custom(self, "sort_by_distance")
        var tile = queue.pop_front()
        tile.visible = true
        var neighs
        if diagonal:
            neighs = neighbors_8(tile.r, tile.c, route_tiles)
        else:
            neighs = neighbors_4(tile.r, tile.c, route_tiles)
        for n in neighs:
            # neighs are route tiles
            var move_tile = move_tiles[n.r][n.c]
            if n.distance == null and \
                    (move_tile.type == TileType.WATER  or move_tile.type == TileType.DEEPWATER) \
                    and move_tile.boat == null:
                var step_distance = abs(n.r - tile.r) + abs(n.c - tile.c)
                if tile.distance + step_distance <= speed:
                    n.distance = tile.distance + step_distance
                    queue.append(n)

# Starting from this (r, c), make all connected move_tiles into an island with the
# island_idx and return a list with these move_tiles.
func unite_island(r, c, island_idx):
    var new_island = []
    var first_tile = move_tiles[r][c]
    if first_tile.type != TileType.LAND or first_tile.island != null:
        return []
    first_tile.island = island_idx
    var queue = [first_tile]
    var next_tile = null
    while len(queue) > 0:
        next_tile = queue.pop_front()
        new_island.append(next_tile)
        var new_neighs = neighbors_4(next_tile.r, next_tile.c, move_tiles)
        for new_tile in new_neighs:
            if new_tile.type == TileType.LAND and new_tile.island == null:
                new_tile.island = island_idx
                queue.append(new_tile)
    return new_island

func unite_islands():
    for r in move_tiles:
        for tile in r:
            tile.island = null
    var new_islands = []
    var island_idx = 0
    for r in G.MAP_DIMS.y:
        for c in G.MAP_DIMS.x:
            var tile = move_tiles[r][c]
            if tile.type == TileType.LAND and tile.island == null:
                var new_island = unite_island(r, c, island_idx)
                if len(new_island) > 0:
                    new_islands.append(new_island)
                    island_idx += 1
    return new_islands

func fix_sprites():
    for r in range(1, G.MAP_DIMS.y - 1):
        for c in range(1, G.MAP_DIMS.x - 1):
            var neighs = area_3x3(r, c, move_tiles)
            var future_neighs = area_3x3(r, c, future_tiles)
            move_tiles[r][c].fix_sprite(neighs)
            future_tiles[r][c].fix_sprite(future_neighs)
    for r in range(G.MAP_DIMS.y):
        for c in range(G.MAP_DIMS.x):
            var water_neigh_types = area_3x3_types(r, c, water_tiles)
            water_tiles[r][c].fix_sprite(water_neigh_types)

func pick_shallows():
    var all_shallows = []
    islands = unite_islands()
    for isl in islands:
        var shallow_is_picked = false
        var shuffled_isl = isl.duplicate()
        shuffled_isl.shuffle()
        for island_tile in shuffled_isl:
            var candidates = neighbors_4(island_tile.r, island_tile.c, move_tiles)
            candidates.shuffle()
            for cand in candidates:
                if cand.r != 0 and cand.r != G.MAP_DIMS.y - 1 \
                        and cand.c != 0 and cand.c != G.MAP_DIMS.x - 1 \
                        and cand.type == TileType.WATER:
                    var future_cand = future_tiles[cand.r][cand.c]
                    if future_cand.type == TileType.WATER:
                        future_cand.set_type(TileType.LAND)
                        all_shallows.append(future_cand)
                        shallow_is_picked = true
                        break
            if shallow_is_picked:
                break
    return all_shallows

func match_now_to_future():
    for r in move_tiles:
        for tile in r:
            var future_tile = future_tiles[tile.r][tile.c]
            if tile.boat == null:
                tile.set_type(future_tile.type)
                if water_tiles[future_tile.r][future_tile.c].fish != null:
                    tile.add_town()

func end_turn():
    if not G.resolving:
        G.resolving = true
        self.current_action = "evolve_islands"

func color_island(island_tiles, color):
    for tile in island_tiles:
        tile.material = OneColorAnimationMaterial.duplicate()
        tile.material.set_shader_param("color", color)
        
func uncolor_islands():
    islands = unite_islands()
    for island in islands:
        for tile in island:
            tile.material = null

func vote():
    G.votes_for = 0
    G.votes_against = 0
    islands = unite_islands()
    for island in islands:
        print(len(island))
        var limit = 3
        for tile in island:
            if tile.partying:
                limit += 1
        if len(island) > limit:
            color_island(island, Color.red)
            G.votes_against += 1
        else:
            color_island(island, Color.green)
            G.votes_for += 1
            G.votes_for_total += 1
    print("IN FAVOR: " + str(G.votes_for))
    print("AGAINST:  " + str(G.votes_against))
    if G.votes_against > G.votes_for:
        G.lost_level = true
        emit_signal("lose")
        print("Voted out of office!")
        set_process(false)
        set_process_input(false)

func reset_game_stuff():
    boats = []
    selected_boat = null

func clear_level_stuff():
    for collection in get_children():
        for thing in collection.get_children():
            if not thing in boats:
                thing.queue_free()
    islands = []
    water_tiles = []
    move_tiles = []
    future_tiles = []
    route_tiles = []
    towns = []
    current_action = "shallows"
    yield(get_tree(), "idle_frame")

func add_new_boats():
    for ability in [BoatAbility.DREDGE, BoatAbility.DRILL, BoatAbility.PARTY]:
        var new_boat = boat_scene.instance()
        new_boat.add_ability(ability)
        $above_land.add_child(new_boat)
        boats.append(new_boat)

func add_new_stuff():
    add_tiles()
    fix_border()
    gen_deepwater()
    add_fish()
    gen_islands()
    place_boats()
    fix_sprites()

func new_game():
    reset_game_stuff()
    add_new_boats()
    start_level()

func start_level():
    clear_level_stuff()
    add_new_stuff()
    self.timey = 0.1
    set_process(true)
    set_process_input(true)

func _ready():
    set_process(false)
    set_process_input(false)
#    new_game()
#    pass

func _process(delta):
    self.timey += delta
    if self.timey > 0.1 and not G.lost_level:
        if self.current_action == "evolve_islands":
            for boat in boats:
                boat.do_actions(self)
            match_now_to_future()
            self.current_action = "vote"
        elif self.current_action == "vote":
            vote()
            if not G.lost_level:
                if G.turn_number == 5:
                    if G.term_number < 4:
                        emit_signal("next_level")
                    else:
                        emit_signal("end_screen")
                        set_process(false)
                        set_process_input(false)
                else:
                    self.current_action = "shallows"
        elif self.current_action == "shallows":
            pick_shallows()
            for boat in boats:
                boat.move_done = false
            self.current_action = "player_turn"
            uncolor_islands()
            G.resolving = false
            G.turn_number += 1
            emit_signal("update_sidebar")
        self.timey = 0.0
        fix_sprites()
