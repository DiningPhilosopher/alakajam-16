extends Node

enum BoatAbility {
    DRILL, # stopping on top of an emerging seabed removes it
        POLAR_SHOCK, # destroy all tiles from north to south, if none have a town
            UBERDRILL, # also remove 4-connected emerging seabeds
    DREDGE, # remove tiles on left and right
        ISLAFORMER, # create rising islands on left and right if isolated
            UBERDRILL, # remove 4-connected tiles
    PARTY, # make towns within 3x3 party
        BONDER, # connect two or more islands on same row/col together for voting purposes
            AROUND_THE_WORLD, # make all towns on this row party
    
    # MOBILITY
    DIAGONAL, # move diagonally at 2 movement cost
    AMPHIBIOUS, # move onto (but don't end on) land at 2 movement cost
    JET # move to any tile in same row/column/diagonal
}
