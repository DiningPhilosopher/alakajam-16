extends Node

var resolving = false
var votes_for = 0
var votes_against = 0
var votes_for_total = 0
var term_number = 1
var turn_number = 1
var lost_level = false

const deepwater_fraction = 0.3
const fish_fraction = 0.3
const game_dims = Vector2(336, 240)

# Size of the entire map, including water borders
const MAP_DIMS = {
    "x": 14,
    "y": 10
}
const TILE_DIM = 24

const DIRS = {
    "u": {
        "x": 0,
        "y": -1
    },
    "d": {
        "x": 0,
        "y": 1
    },
    "l": {
        "x": -1,
        "y": 0
    },
    "r": {
        "x": 1,
        "y": 0
    }
}
