extends TextureRect

onready var turn_number = $VBoxContainer/CenterContainer8/Year
onready var term_number = $VBoxContainer/CenterContainer7/Term
onready var score = $VBoxContainer/CenterContainer6/score

func _ready():
    for boat in [$VBoxContainer/dredger,
            $VBoxContainer/driller,
            $VBoxContainer/party]:
        boat.rect_min_size.y = 16
    for boat in [$VBoxContainer/dredger/AnimatedSprite,
            $VBoxContainer/driller/AnimatedSprite,
            $VBoxContainer/party/AnimatedSprite]:
        boat.scale = Vector2(0.75, 0.75)
        boat.position.x += 20
        boat.position.y += 8

func update_sidebar():
    term_number.bbcode_text = "term: " + str(G.term_number)
    turn_number.bbcode_text = "year: " + str(G.turn_number)
    score.bbcode_text = "score: " + str(G.votes_for_total)
