extends TextureRect

onready var text_label = $HBoxContainer/VBoxContainer/RichTextLabel



func opening_screen():
    text_label.rect_min_size.x = 2600
    text_label.bbcode_text = """Presidente!\n
In the wake of sweeping and decisive action by world leaders at the G20 summit, greenhouse emissions have been reduced so much that the sea level is now rapidly falling!

Naturally, your constituency is upset and expects you to preserve their way of life on small, individual islands. In your district voting system, each island has one vote. Islands that exceed size 4 will vote against you, smaller island will naturally support you.

Use your nations fleet to [color=#808080]drill[/color], [color=red]dredge[/color] and [color=#4169E1]party[/color] your way through five five-year terms!

Your legacy will be measured in the [color=red]total number of support votes[/color] you received.
"""
    self.visible = true
    set_popup_size(Vector2(300, 220))
    yield(get_tree(), "idle_frame")
    text_label.rect_scale = Vector2(0.1, 0.1)

func new_game():
    text_label.bbcode_text = ""
    self.visible = false

func lose_game():
    text_label.rect_min_size.x = 1750
    text_label.bbcode_text = """Dear Governor,\n
We regret to inform you that the vote went against you.\n
Support: """ + str(G.votes_for) + "\n" + \
"Opposition: " + str(G.votes_against) + "\n \n" + \
"""We trust you will find employment elsewhere.\n \n \n
[color=red]Total votes secured: """ + str(G.votes_for_total) + "[/color]"
    self.visible = true
    set_popup_size(Vector2(200, 170))
    yield(get_tree(), "idle_frame")
    text_label.rect_scale = Vector2(0.1, 0.1)

func end_screen():
    text_label.rect_min_size.x = 1750
    text_label.bbcode_text = """Congratulations!\n
For the past 25 years, you have kept the trust of your citizens.\n\n
[color=red]Total votes secured: """ + str(G.votes_for_total) + "[/color]"
    self.visible = true
    set_popup_size(Vector2(200, 110))
    yield(get_tree(), "idle_frame")
    text_label.rect_scale = Vector2(0.1, 0.1)

# Changes the size of the popup menu (background)
# and recenters it.
func set_popup_size(size_vec):
    self.rect_size = size_vec
    self.rect_position = 0.5 * (G.game_dims - self.rect_size)
